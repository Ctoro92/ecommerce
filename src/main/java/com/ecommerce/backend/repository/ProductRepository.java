package com.ecommerce.backend.repository;

import com.ecommerce.backend.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {


    Product findAllById(Long id);
}
