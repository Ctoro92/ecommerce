package com.ecommerce.backend.repository;

import com.ecommerce.backend.models.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Integer> {
    Cart findAllById(Long id);
    Cart findAllByIdAndPaymentStatus(Long id,String PaymentStatus);
    
}
