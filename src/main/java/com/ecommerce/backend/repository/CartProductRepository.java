package com.ecommerce.backend.repository;

import com.ecommerce.backend.models.CartProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CartProductRepository extends CrudRepository<CartProduct, Integer> {

    List<CartProduct> findAllByCartId(Long id);
    @Transactional
    void deleteAllByCartId(Long id);

}
