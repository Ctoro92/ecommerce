package com.ecommerce.backend.services;

import com.ecommerce.backend.exception.CartException;
import com.ecommerce.backend.models.CartProduct;

import java.util.List;
import java.util.Optional;

public interface CartProductService {

    List<CartProduct> getCartProductById(Long id) throws CartException;
    Optional<CartProduct> getCartProduct(Long id);
    void save(List<CartProduct> cartProduct);

}
