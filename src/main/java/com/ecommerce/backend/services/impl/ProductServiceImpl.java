package com.ecommerce.backend.services.impl;

import com.ecommerce.backend.dtos.ProductDTO;
import com.ecommerce.backend.exception.ProductException;
import com.ecommerce.backend.models.Product;
import com.ecommerce.backend.repository.ProductRepository;
import com.ecommerce.backend.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;
    @Override
    public void createProduct(ProductDTO productDTO) {

    }

    @Override
    public Product getProductFromId(Long id) throws ProductException{
        try {
            Product product = productRepository.findAllById(id);
            if (ObjectUtils.isEmpty(product)){
                throw new ProductException(HttpStatus.NOT_FOUND.value(),"[METHOD] getCartActiveById [MESSAGE] Este producto no existe");
            }
            return product;
        }catch (Exception ex){
            throw new ProductException(HttpStatus.NOT_FOUND.value(),ex.getMessage());
        }
    }

    @Override
    public void deleteProductFromId(Long id) {

    }

    @Override
    public void updateProduct(ProductDTO product) {

    }
}
