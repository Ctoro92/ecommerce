package com.ecommerce.backend.services.impl;

import com.ecommerce.backend.exception.CartException;
import com.ecommerce.backend.models.CartProduct;
import com.ecommerce.backend.repository.CartProductRepository;
import com.ecommerce.backend.services.CartProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class CartProductServiceImpl implements CartProductService {


    private CartProductRepository cartProductRepository;


    @Autowired
    public CartProductServiceImpl(CartProductRepository cartProductRepository) {
        this.cartProductRepository = cartProductRepository;

    }


    @Override
    public List<CartProduct> getCartProductById(Long id) throws CartException{
        try {
            return cartProductRepository.findAllByCartId(id);
        }catch (Exception ex){
            throw new CartException(HttpStatus.INTERNAL_SERVER_ERROR.value(),ex.getMessage());
        }

    }

    @Override
    public Optional<CartProduct> getCartProduct(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(List<CartProduct> cartProduct) {
        cartProductRepository.saveAll(cartProduct);
    }
}
