package com.ecommerce.backend.services.impl;

import com.ecommerce.backend.exception.CartException;
import com.ecommerce.backend.models.Cart;
import com.ecommerce.backend.repository.CartProductRepository;
import com.ecommerce.backend.repository.CartRepository;
import com.ecommerce.backend.services.CartService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Timer;
import java.util.TimerTask;

@Service
public class CartServiceImpl implements CartService {
    private static final Logger LOGGER = Logger.getLogger(CartServiceImpl.class);
    private CartRepository cartRepository;
    private CartProductRepository cartProductRepository;
    private static final String NOT_PAID =  "not_paid";

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, CartProductRepository cartProductRepository) {
        this.cartRepository = cartRepository;
        this.cartProductRepository = cartProductRepository;
    }

    @Override
    public Cart getCartById(Long id) throws CartException{

        try {
            Cart cart = cartRepository.findAllById(id);
            if (ObjectUtils.isEmpty(cart)){
                throw new CartException(HttpStatus.NOT_FOUND.value(),"[METHOD] getCartById [MESSAGE] Este carrito no existe");
            }
            return cart;
        }catch (Exception ex){
            LOGGER.error("[METHOD] getCartById [ERROR]" + ex.getMessage());
            throw new CartException(HttpStatus.NOT_FOUND.value(),ex.getMessage());
        }
    }

    @Override
    public Cart getCartActiveById(Long id) throws CartException {
        try {
            return cartRepository.findAllByIdAndPaymentStatus(id,NOT_PAID);
        }catch (Exception ex){
            LOGGER.error("[METHOD] getCartActiveById [MESSAGE] Error al recuperar un carrito activo");
            throw new CartException(HttpStatus.NOT_FOUND.value(),ex.getMessage());
        }
    }

    @Override
    public Cart createCart() {
        Cart cart = cartRepository.save(new Cart());
        borradoDeCarritoNoPagado(cart);
        return cart;
    }

    @Override
    public void updateCart(Cart cart) {
        cartRepository.save(cart);
    }

    private void borradoDeCarritoNoPagado(Cart cart){
        Timer timer = new Timer();
        TimerTask tarea = new TimerTask() {
            @Override
            public void run() {
                Cart cartToDelete = cartRepository.findAllByIdAndPaymentStatus(cart.getId(),NOT_PAID);
                if(!ObjectUtils.isEmpty(cartToDelete)){
                    cartProductRepository.deleteAllByCartId(cartToDelete.getId());
                    cartRepository.delete(cartToDelete);
                }
            }
        };
        //TTL 10m
        timer.schedule(tarea,600000);
    }
}
