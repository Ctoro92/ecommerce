package com.ecommerce.backend.services;

import com.ecommerce.backend.exception.CartException;
import com.ecommerce.backend.models.Cart;

public interface CartService {

    Cart getCartById(Long id) throws CartException;
    Cart getCartActiveById(Long id) throws CartException;
    Cart createCart();
    void updateCart(Cart cart);

}
