package com.ecommerce.backend.dtos;

public class CartProductDTO {
    private Long productId;
    private int quantity;
    private Long cartId;
}
