package com.ecommerce.backend.dtos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CartDTO {

    private Long id;
    private LocalDate creationDate;
    private Double cost;
    private String paymentStatus;

    public CartDTO(Long id, LocalDate creationDate, Double cost, String paymentStatus) {
        this.id = id;
        this.creationDate = creationDate;
        this.cost = cost;
        this.paymentStatus = paymentStatus;
    }
}
