package com.ecommerce.backend.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class ProductDTO {

    private HashMap<Long, Integer> productIdAndQuantityMap;
    private Long cartId;
}
