package com.ecommerce.backend.handler;

import com.ecommerce.backend.dtos.ProductDTO;
import com.ecommerce.backend.exception.ProductException;
import com.ecommerce.backend.models.Cart;
import com.ecommerce.backend.models.CartProduct;
import com.ecommerce.backend.models.Product;
import com.ecommerce.backend.services.CartProductService;
import com.ecommerce.backend.services.CartService;
import com.ecommerce.backend.services.ProductService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class ProductHandler {

    private static final Logger LOGGER = Logger.getLogger(ProductHandler.class);
    private CartService cartService;
    private CartProductService cartProductService;
    private ProductService productService;


    @Autowired
    public ProductHandler(CartService cartService, CartProductService cartProductService, ProductService productService) {
        this.cartService = cartService;
        this.cartProductService = cartProductService;
        this.productService = productService;
    }




    public void addProductsToCart(ProductDTO productDTO) throws ProductException{
        Cart cart;
        List<CartProduct> cartProducts;
        try {
            //hay un carrito activo(no pagado y con TTL < 10m)?
            cart = cartService.getCartActiveById(productDTO.getCartId());
            if(ObjectUtils.isEmpty(cart)){
                cart = cartService.createCart();
            }
            cartProducts = cartProductService.getCartProductById(productDTO.getCartId());
            addNewProducts(cartProducts, productDTO.getProductIdAndQuantityMap(), cart);

        }catch (Exception ex){
            LOGGER.error("[METHOD] addProductsToCart [MESSAGE] error inesperado");
            throw new ProductException(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        }
    }

    private void addNewProducts(List<CartProduct> cartProducts,HashMap<Long, Integer> productsToAdd, Cart cart) throws ProductException {
        productsToAdd.forEach((k,v) -> {
            try {
                Product pd = productService.getProductFromId(k);
                CartProduct cp = new CartProduct();
                cp.setProduct(pd);
                cp.setQuantity(v);
                cp.setCreationDate(new Date());
                cart.setCost(cart.getCost() + v * cp.getProduct().getAmount());
                cp.setCart(cart);
                cartProducts.add(cp);

            }catch (ProductException ex){
                LOGGER.error("[METHOD] addNewProducts [MESSAGE] Error añadiendo productos al carrito");
            }

        });
        cartProductService.save(cartProducts);
    }

}
