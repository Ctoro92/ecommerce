package com.ecommerce.backend.controllers;

import com.ecommerce.backend.dtos.ProductDTO;
import com.ecommerce.backend.exception.ProductException;
import com.ecommerce.backend.handler.ProductHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductsController {


    private ProductHandler productHandler;

    @Autowired
    public ProductsController(ProductHandler productHandler) {
        this.productHandler = productHandler;
    }

    @PostMapping(value = "/addProductsToCart", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity addProductToCart(@RequestBody ProductDTO productDTO) throws ProductException {
        this.productHandler.addProductsToCart(productDTO);
        return new ResponseEntity(HttpStatus.OK);
    }
}
