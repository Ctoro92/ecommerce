package com.ecommerce.backend.controllers;

import com.ecommerce.backend.dtos.CartDTO;
import com.ecommerce.backend.exception.CartException;
import com.ecommerce.backend.models.Cart;
import com.ecommerce.backend.models.CartProduct;
import com.ecommerce.backend.services.CartProductService;
import com.ecommerce.backend.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/cart")
public class CartController {


    private CartProductService cartProductService;
    private CartService cartService;

    @Autowired
    public CartController(CartProductService cartProductService, CartService cartService) {
        this.cartProductService = cartProductService;
        this.cartService = cartService;
    }


    @GetMapping(value = "/cartInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<CartProduct>> getCartInfo (@RequestBody CartDTO cartDTO) throws CartException{
        return new ResponseEntity<>(cartProductService.getCartProductById(cartDTO.getId()), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(value = "/createCart", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Cart> createCart(){
        return new ResponseEntity<>(cartService.createCart(),new HttpHeaders(), HttpStatus.OK);
    }
    @PostMapping(value = "/payCart", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity payCart(@RequestBody CartDTO cartDTO) throws CartException {
        Cart cartToUpdate = cartService.getCartById(cartDTO.getId());
        cartToUpdate.setPaymentStatus("paid");
        cartService.updateCart(cartToUpdate);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteCart", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity deleteCart(@RequestBody CartDTO cartDTO) throws CartException {
        Cart cartToUpdate = cartService.getCartById(cartDTO.getId());
        cartToUpdate.setDeleted(true);
        cartService.updateCart(cartToUpdate);
        return new ResponseEntity(HttpStatus.OK);
    }
}
