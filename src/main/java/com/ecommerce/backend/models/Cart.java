package com.ecommerce.backend.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private double cost;

    private Date creationDate;

    private String  paymentStatus;

    private boolean deleted;

    public Cart() {
        this.creationDate = new Date();
        this.paymentStatus = "not_paid";
    }

}
